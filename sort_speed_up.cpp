#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

void printVector(vector <int> array);

int main(){
	long long N;
	scanf("%lld",&N);

	/*Input vector*/
	vector <int> sin;
	int temp;
	for(int i=0;i<N;++i){
		scanf(" %d",&temp);
		sin.push_back(temp);
	}

	sort(sin.begin(),sin.end());
	printVector(sin);
}

void printVector(vector <int> array){
	for(int i=0;i<array.size();++i)
		printf("%d ",array.at(i));
	printf("\n");
}
